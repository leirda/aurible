; SPDX-FileCopyrightText: 2021 leirda <leirda@aurible.fr>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(import (aurible))

(define navigation
  '((informatique "#Informatique")
    (pro          "#Expériences professionnelles")
    (autre        "#Autres centres d’intérêts")))

(define ressources
  `((évènement
      (conf-manu "https://emmanuellecournari.wixsite.com/conf-gest")
      (proj-nth ,(++ "https://www.franciliens.net/2019/01/projection-debat-de"
                     "nothing-to-hide-au-lycee-autogere-de-paris"))
      (gogodot "https://gogodotjam.com")
      (gmtk2020 "https://itch.io/jam/gmtk-2020"))
    (org 
      (lap "https://www.l-a-p.org") 
      (ut7 "http://ut7.fr"))
    (projet
      (saisie "https://codeberg.org/leirda/saisie")
      (owc "https://one-winged-craft.itch.io"))
    (logiciel
      (saisie "https://codeberg.org/leirda/saisie")
      (godot "https://godotengine.org"))
    (langage
      (scratch "https://scratch.mit.edu/")
      (snap! "https://snap.berkeley.edu/"))
    (linux
      (archlinux "https://archlinux.org/"))))

(define page
  (with
    "CV — Adriel Dumas--Jondeau"
    navigation
    (resume "Je suis fasciné par l’informatique depuis que je la découvre. Je "
            "souhaite explorer toujours davantage les possiblités qu’ouvre ce "
            "domaine, qu’elles soient techniques, pédagogiques ou politiques.")

    (section
      "Informatique"
      (» (text "2021-… :"
                (» (text "Apprentissage de plusieurs langages de programmation "
                          "(Python, Javascript, C, R, GDScript…).")
                   (text "Découverte et apprentissage de langages plus… "
                         "exotiques (Elm, Common Lisp, Scheme, Zig…).")
                   (text "Apprentissage d’outils d’administration système et "
                         "réseau (LVM, Systemd, Docker, Caddy, Dnsmasq…).")))

         (text "2017-… :"
               (» (text "Initiation à l’interface en ligne de commande (avec "
                         "Bash, Fish, Vim, SSH, Git…).")
                  (text "Apprentissage du C, langage avec lequel je découvre "
                        "les notions fondatrices des systèmes GNU/Linux.")
                  (text "Première installation et découverte d’un système sous "
                        (url "GNU/Linux"
                             (from ressources '(linux archlinux))) ".")))

         (text "2016 : Découverte et adhésion au mouvement du logiciel libre.")

         (text "2015 : Initiation au langage de programmation "
               (url "Scratch" (from ressources '(langage scratch)))
               " introduit en cours de mathématique (collège).")))

    (section
      "Expériences professionnelles"
      (» (text "2020-21 : Différentes expériences en auto-entrepreneur :"
                (» (text "Création de sites Web en WordPress.")
                   (text "Animation d’ateliers d’initiation à l’informatique "
                         "pour les enfants, en journée des associations.")
                   (text "Rédaction, mise en page et publication d’articles "
                         "et de contenus pour une association.")
                   (text "Création d’un logiciel libre de "
                         (url "saisie" (from ressources '(logiciel saisie)))
                         " et de traitement de données pour une association "
                         "dans le domaine du travail social.")))

         (text "2019-20 : Animation d’ateliers "
               (url "Scratch" (from ressources '(langage scratch)))
               " pour des enfants (8-12 ans) en association.")

         (text "2019 :"
               (» (text "Animation en colonie de vacances (8-12 ans).")
                  (text "Animation en centre de loisirs (3-12 ans).")
                  (text "Réalisation d’un stage spontanée au lycée chez "
                        (url "la coopérative /ut7"
                             (from ressources '(org ut7))) ".")))

         (text "2018-19 : Organisation d’évènements au "
               (url "lycée autogéré de Paris" (from ressources '(org lap)))
               (» (text "Représentation par Emmanuelle Cournarie d’une "
                         (url "conférence gesticulée"
                              (from ressources '(évènement conf-manu)))
                          " sur le monde du travail.")
                  (text (url "Projection-débat"
                             (from ressources '(évènement proj-nth)))
                        " du film documentaire « Nothing To Hide ».")))))

    (section
      "Autres centres d’intérêts"
      (» (text "Je pratique la randonnée depuis l’enfance, en métropole et à "
                "l’île de la Réunion.")
         (text "J’ai pratiqué le piano et le violon pendant respectivement "
               "13 et 3 ans.")
         (text "Je commence à peine à m’initier au yoga qui est une vraie "
               "révélation pour moi depuis une retraite avec des ami·e·s.")
         (text "Je pratique doucement le jonglage et souhaiterait m’initier "
               "à d’autres pratiques circaciennes sur mon temps libre.")))))


(let ((this "cv.scm"))
 (render this page))
