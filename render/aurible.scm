; SPDX-FileCopyrightText: 2021 leirda <leirda@aurible.fr>
;
; SPDX-License-Identifier: GPL-3.0-or-later

;; Procedure: *from* alist & keys
;; Returns the value associated with keys, recursively going through alist.
;;
;; (get '((a b) (b ((c d) (e f))) (g h)) 'b 'e) => f

;; Procedure: *url* desc url
;; Thin wrapper to generate a valid SXML <href>
;;
;; (url "perdu" "perdu.com") => `(a (@ (href "perdu.com")) "perdu")

;; Procedure: *render* input-file with-content
;; Write an HTML representation of input-file to an output file named after
;; input-file by replacing `.scm` by `.html`.

;; Procedure: *resume* lines
;; Returns a SXML resume of a document with lines.

;; Procedure: *section* title . content
;; Returns a section with title encapsulating content. It returns an `h2` title
;; followed by a `div` with a title class.

;; Procedure: *text* lines
;; Thin wrapper to generate a valid SXML <div>.
;;
;; (text "hello, " "World!") => `(div "hello, " "World!")

;; Procedure: *with* title toc & sxml
;; Returns an HTML representation of SXML's expression. It's a thin wrapper
;; around sxml->html from (html-parser) but adds <title> and proper <head>.
;; It also add a toc at the top of the page which is an a list.

;; Procedure: *»* items
;; Thir wrapper to generate a valid SXML <ul> and its children.

(module aurible
  (from url render resume section text with ++ »)
(import scheme
        (chicken base)
        (chicken io)
        (chicken string)
        (colorize)
        (html-parser)
        (pipes))


(define (render input-file with-content . toc)
  (let ((output-file
          (string-translate* input-file '((".scm" . ".html")))))
    (pipe input-file
          (file->string)
          (source->sxml)
          (with-content)
          (sxml->html)
          (string->file output-file))))


;; generic utils


(define (from alist keys)
  (if (= (length keys) 0)
      (car alist)
      (from
        (alist-ref (car keys) alist)
        (cdr keys))))


(define (++ . strings)
  (apply string-append strings))


;; html utils


(define (with title toc . body)
  (lambda (source)
    `(*TOP*
       (html
         (head
           (title ,title)
           (meta
             (@ (http-equiv "Content-Type")
                (content "text/html")
                (charset "UTF-8")))
           (link
             (@ (rel "stylesheet")
                (type "text/css")
                (href "./theme.css"))))
         (body
           ,(if (> (length toc) 0)
                `(nav ,(map nav toc)) '())
           (div (@ (class "page"))
                (div (@ (class "source")) ,source)
                (article (h1 ,title) ,@body)))))))


(define (source->sxml source)
  `(pre
     (code
       ,(pipe source
              ((flip* html-colorize) 'scheme)
              (html->sxml)))))


(define (nav pair)
  (let ((desc (car pair))
        (uri (cadr pair)))
    `(span (@ (class "nav-item"))
          ,(url desc uri))))


(define (text . lines)
  `(p ,@lines))

(define (resume . lines)
  `(div (@ (class "resume")) ,@lines))

(define (section title . content)
  `((h2 (@ (id ,title)) ,title)
    (div (@ (class ,title)) ,@content)))

(define (url desc uri)
  `(a (@ (href ,uri)) ,desc))


(define (» . items)
  (let ((item (lambda (item)
                `(li ,item))))
    `(ul ,(map item items))))


;; i/o utils

(define (file->string input-file)
  (call-with-input-file
    input-file
    (lambda (port) (read-string #f port))))


(define (string->file string output-file)
  (call-with-output-file
    output-file
    (lambda (port) (write-string string #f port)))))
