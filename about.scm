; SPDX-FileCopyrightText: 2021 leirda <leirda@aurible.fr>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(import (aurible))

(define ressources
  '((licence "GPLv3")
    (sources "/sources.tar.gz")))

(define page
  (with "À propos" '()
        (text "Les " (url "sources" (from ressources '(sources)))
              " de ce site sont sous " (from ressources '(licence)) ".")))

(let ((this "about.scm"))
 (render this page))
