<!--
SPDX-FileCopyrightText: 2021 leirda <leirda@aurible.fr>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Aurible

Le principe c’est que le code qui permet de générer la page, c’est la page.

Pour l’instant, va y avoir essentiellement mon CV, mais je compte bien y ajouter
des choses personnelles à l’avenir, histoire d’avoir un joli site…
