# SPDX-FileCopyrightText: 2021 leirda <leirda@aurible.fr>
#
# SPDX-License-Identifier: GPL-3.0-or-later

.PHONY: clean aurible


all: aurible content

aurible: render/aurible.scm
	cd render && chicken-install -sudo

tar: Makefile LICENSES .git* render/aurible.egg render/aurible.scm *.scm *.md *.css
	tar caf sources.tar.gz $^ --transform 's#^#sources/#'

release: tar aurible content
	zip release.zip *.html *.css *.pub *.asc *.tar.gz
	

clean:
	rm --recursive --force *.html *.tar.gz

content: *.scm
	chicken-csi $? -eval '(exit)'
