; SPDX-FileCopyrightText: 2021 leirda <leirda@aurible.fr>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(import (aurible))

(define navigation
  '((accueil "https://aurible.fr/")
    (cv      "https://aurible.fr/cv.html")
    (apropos "https://aurible.fr/about.html")
    (sources "https://aurible.fr/sources.tar.gz")
    (contact (mail "mailto:leirda@aurible.fr")
             (xmpp "xmpp:leirda@jabber.fr")
             (opgp "/leirda.pub.asc"))))

(define main-page
  (with "Bienvenue !"
        (butlast navigation)
        (text "Ce site est en cours de construction, vous n’y trouverez pas "
              "grand chose pour le moment. Je compte l’étoffer avec le "
              "temps ; y ajouter du contenu, des fonctionnalités…")

        (text "Mais il faut bien commencer par quelque chose ! :-)")

        (text "Quand j’en aurai le temps, vous pourrez trouver ici différentes "
              "ressources que j’ai pu découvrir, produire, ou auxquelles j’ai "
              "pu participer.")

        (text "Vous trouverez également des services que je mets à disposition "
              "sur mon temps libre (partage de fichiers, documents "
              "collaboratifs, messagerie instantanée…)")

        (text "En attendant, vous pouvez consulter mon cv "
              (url "ici" (from navigation '(cv))) ".")

        (text "Vous pouvez également me joindre"
              " par "    (url "mail" (from navigation '(contact mail)))
              " ou par " (url "xmpp" (from navigation '(contact xmpp)))
              " avec "   (url "cette clef OpenPGP"
                              (from navigation '(contact opgp))) ".")))


(let ((this "index.scm"))
 (render this main-page))
